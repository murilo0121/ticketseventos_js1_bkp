package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

public class Setor {
	
	private Long id;
	private String nome;
	private int capacidade;
	private Double preco;
	
	public Setor(Long id, String nome, int capacidade, Double preco) {
		this.id = id;
		this.nome = nome;
		this.capacidade = capacidade;
		this.preco = preco;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getCapacidade() {
		return capacidade;
	}
	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
	

}
