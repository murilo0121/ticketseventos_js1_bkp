package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.Date;

public abstract class Evento {
	
	private Long id;
	private String nome;
	private String descricao;
	private Date dataEvento;
	private int censura;
	private double comissao;
	
	
	
	public Evento(Long id, String nome, String descricao, Date dataEvento,
			int censura, double comissao) {
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.dataEvento = dataEvento;
		this.censura = censura;
		this.comissao = comissao;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Date getDataEvento() {
		return dataEvento;
	}
	public void setDataEvento(Date dataEvento) {
		this.dataEvento = dataEvento;
	}
	public int getCensura() {
		return censura;
	}
	public void setCensura(int censura) {
		this.censura = censura;
	}
	public double getComissao() {
		return comissao;
	}
	public void setComissao(double comissao) {
		this.comissao = comissao;
	}

	
	
}
