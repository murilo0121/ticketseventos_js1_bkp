package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.Date;

public class Venda {

	private long id;
	private Date dataVenda;
	private Pessoa pessoa;
	
	public Venda(long id, Date dataVenda, Pessoa pessoa) {
		this.id = id;
		this.dataVenda = dataVenda;
		this.pessoa = pessoa;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getDataVenda() {
		return dataVenda;
	}
	public void setDataVenda(Date dataVenda) {
		this.dataVenda = dataVenda;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
}
