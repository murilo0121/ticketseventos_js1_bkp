package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

/**
 * Classe que representa a abstracao de um artista
 * @author mauda
 *
 */

//Murilo Erhardt
public class Artista {
 
	private Long id;
	private String nome;
	
	public Artista() {
		System.out.println("ok");
	}
	
	public Artista(Long id, String nome) {
		this.id = id;
		this.nome = nome;
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		System.out.println("okssd");
		this.nome = nome;
	}

	@Override
	public boolean equals(Object obj) {
		System.out.println("okss");
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artista other = (Artista) obj;
		if (id == null || !id.equals(other.id))
			return false;
		return true;
	}	

	public void deletarArtistaCompleto(Artista objeto){
		objeto = null;
	}
}
 
