package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.Date;

public class Filme extends Evento{

	private String genero;
	
	public Filme(Long id, String nome, String descricao, Date dataEvento, int censura, double comissao, String genero) {
		super(id, nome, descricao, dataEvento, censura, comissao);
		this.setGenero(genero);
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}
	
}
