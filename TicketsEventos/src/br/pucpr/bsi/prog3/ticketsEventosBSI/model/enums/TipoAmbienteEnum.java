package br.pucpr.bsi.prog3.ticketsEventosBSI.model.enums;

public enum TipoAmbienteEnum {

	c("cinema"), s("casashow"), t("teatro");
	
	private String nome;
	
	private TipoAmbienteEnum(String nome){
		this.nome = nome;
	}

	public String getDescricao() {
		return nome;
	}

	public void setDescricao(String descricao) {
		this.nome = descricao;
	}
	
}
