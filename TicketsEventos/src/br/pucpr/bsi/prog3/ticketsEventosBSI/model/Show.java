package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.Date;

public class Show extends Evento{
	
	private String estilo;
	
	public Show(Long id, String nome, String descricao, Date dataEvento, int censura, double comissao, String estilo){
		super(id, nome, descricao, dataEvento, censura, comissao);
		this.setEstilo(estilo);
	}

	
	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	
	

}
