package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.Date;

public class Peca extends Evento{

	private String companhia;
	private String genero;
	
	public Peca(Long id, String nome, String descricao, Date dataEvento,
			int censura, double comissao, String companhia, String genero) {
			super(id, nome, descricao, dataEvento, censura, comissao);
			this.companhia = companhia;
			this.genero = genero;
	}

	public String getCompanhia() {
		return companhia;
	}

	public void setCompanhia(String companhia) {
		this.companhia = companhia;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}
	
}
