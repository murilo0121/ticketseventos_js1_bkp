package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

public class Ingresso {
	
	private long id;
	private Setor setor;
	private double preco;
	
	public Ingresso(long id, Setor setor, double preco) {
		super();
		this.id = id;
		this.setor = setor;
		this.preco = preco;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Setor getSetor() {
		return setor;
	}
	public void setSetor(Setor setor) {
		this.setor = setor;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	

}
