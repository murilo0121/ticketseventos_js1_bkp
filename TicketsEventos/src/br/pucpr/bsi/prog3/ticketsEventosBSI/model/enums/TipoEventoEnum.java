package br.pucpr.bsi.prog3.ticketsEventosBSI.model.enums;

public enum TipoEventoEnum {
	
	p("peca"), f("filme"), s("show");
	
	private String descricao;
	
	private TipoEventoEnum(String descricao){
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
