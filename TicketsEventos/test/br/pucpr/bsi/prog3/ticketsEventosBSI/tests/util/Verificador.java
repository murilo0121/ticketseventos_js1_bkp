package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.util;

import org.junit.Assert;

import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;

/**
 * Classe responsavel por atualizar informacoes para update e comparacoes
 * @author Mauda
 *
 */
public class Verificador {
	
	public static void verificar(Artista objetoBD, Artista objeto){
		Assert.assertEquals(objetoBD.getNome(),				objeto.getNome());
	}
	
}
